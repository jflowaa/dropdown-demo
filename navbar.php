<!-- This is the navbar that is at the top of every page, it is called to each page
via "include ( 'navbar.php');" reduces code/more oragnized -->
<?php $root='http://localhost/aozone/pages/' ; ?>
<div class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <a href="<?php echo $root; ?>index.php" class="navbar-brand">AOzone</a>
            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="navbar-collapse collapse" id="navbar-main">
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes">Nanos
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="themes">
                        <li>
                            <a href="<?php echo $root; ?>nanos/_generic.php">Generic</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="<?php echo $root; ?>nanos/adventurer.php">Adventurer</a>
                        </li>
                        <li>
                            <a href="<?php echo $root; ?>nanos/agent.php">Agent</a>
                        </li>
                        <li>
                            <a href="<?php echo $root; ?>nanos/bureaucrat.php">Bureaucrat</a>
                        </li>
                        <li>
                            <a href="<?php echo $root; ?>nanos/doctor.php">Doctor</a>
                        </li>
                        <li>
                            <a href="<?php echo $root; ?>nanos/enforcer.php">Enforcer</a>
                        </li>
                        <li>
                            <a href="<?php echo $root; ?>nanos/engineer.php">Engineer</a>
                        </li>
                        <li>
                            <a href="<?php echo $root; ?>nanos/fixer.php">Fixer</a>
                        </li>
                        <li>
                            <a href="<?php echo $root; ?>nanos/keeper.php">Keeper</a>
                        </li>
                        <li>
                            <a href="<?php echo $root; ?>nanos/matrial_artist.php">Matrial Artist</a>
                        </li>
                        <li>
                            <a href="<?php echo $root; ?>nanos/meta-physicist.php">Meta-Physicist</a>
                        </li>
                        <li>
                            <a href="<?php echo $root; ?>nanos/nano-technician.php">Nano-Technician</a>
                        </li>
                        <li>
                            <a href="<?php echo $root; ?>nanos/shade.php">Shade</a>
                        </li>
                        <li>
                            <a href="<?php echo $root; ?>nanos/soldier.php">Soldier</a>
                        </li>
                        <li>
                            <a href="<?php echo $root; ?>nanos/trader.php">Trader</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes">Charater Tools
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="themes">
                        <li>
                            <a href="#">Setup Builder</a>
                        </li>
                        <li>
                            <a href="#">Skill Emulator</a>
                        </li>
                        <li>
                            <a href="#">Perk Config</a>
                        </li>
                        <li>
                            <a href="#">Item Database</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes">Guides
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="themes">
                        <li>
                            <a href="#">Rubi-Ka</a>
                        </li>
                        <li>
                            <a href="#">Shadowlands</a>
                        </li>
                        <li>
                            <a href="#">Lost Eden</a>
                        </li>
                    </ul>
                </li>
                <form class="navbar-form navbar-left">
                    <input type="text" class="form-control col-lg-8" placeholder="Search an Item...">
                </form>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="http://forums.anarchy-online.com/" target="_blank">Offical Forums</a>
                </li>
            </ul>
        </div>
    </div>
</div>
